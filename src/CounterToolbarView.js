import React, {Component} from 'react';
import {actionCreators} from './counterStore';

class CounterToolbarView extends Component  {
    increase() {
        if (this.props.increaseAction) {
            this.props.increaseAction(this.props.counter);
        }
    }

    decrease() {
        if (this.props.decreaseAction) {
            this.props.decreaseAction(this.props.counter);
        }
    }

    render() {
        return (
            <p> [{this.props.counter}]&nbsp;
                <button onClick={this.increase.bind(this)}>Öka</button>
                <button onClick={this.decrease.bind(this)}>Minska</button>
            </p>
        );
    }
}
CounterToolbarView.propTypes = {
    counter: React.PropTypes.string.isRequired,
    increaseAction : React.PropTypes.func.isRequired,
    decreaseAction : React.PropTypes.func.isRequired
}

export default CounterToolbarView;
