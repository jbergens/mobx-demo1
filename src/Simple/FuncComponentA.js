import React, { Component } from 'react';

function FuncComponentA(props) {
    return (
        <div>
            <p>Hej {props.name}! </p>
            <p>Detta är en stateless component. </p>
        </div>
    );
}

export default FuncComponentA;
