import React, { Component } from 'react';

const myStyles = {
    border: 'solid 1px black',
    paddingLeft: 10,
    paddingRight: 10
};

const FuncComponentB = ({name}) => {
    return (
        <div style={myStyles}>
            <p>Hej {name}! </p>
            <p>Detta är en stateless component med style.</p>
        </div>
    );
}

export default FuncComponentB;
